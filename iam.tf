data "aws_iam_policy_document" "this" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

# Execution Role
resource "aws_iam_role" "this" {
  name               = "${var.app_name}-task-exec-role"
  description        = "Permissions to startup the container"
  assume_role_policy = data.aws_iam_policy_document.this.json
}

# AmazonECSTaskExecutionRolePolicy
resource "aws_iam_role_policy_attachment" "this" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.this.id
}

# Custom policies for execution role. tip: SSM and KMS secrets policies
resource "aws_iam_role_policy_attachment" "exec" {
  count      = length(var.iam_policies_for_execution)
  role       = aws_iam_role.this.name
  policy_arn = element(var.iam_policies_for_execution, count.index)
}

# Task Role
resource "aws_iam_role" "task" {
  name               = "${var.app_name}-task-perms-role"
  description        = "Permissions to access aws services from within the container"
  assume_role_policy = data.aws_iam_policy_document.this.json
}

resource "aws_iam_role_policy_attachment" "permissions" {
  count      = length(var.iam_policies)
  role       = aws_iam_role.task.name
  policy_arn = element(var.iam_policies, count.index)
}
